import './App.css';
import axios from 'axios';
import { ThemeProvider } from 'styled-components';
import { GlobalStyles } from './globalStyles';
import { useEffect, useState } from 'react';
import { BrowserRouter as Router, Link, Route, Routes } from 'react-router-dom';
import { AwesomeButton as Button } from 'react-awesome-button';
import pic from './image.png';
import 'react-awesome-button/dist/styles.css';

function App() {
	// API config
	const config = { headers: { Accept: 'application/json', Authorization: 'Bearer 1MKl2KScejiFK35UC3hF' } };
	const apiUrl = 'https://the-one-api.dev/v2/';

	// Dark/Light mode config
	const [theme, setTheme] = useState(true);
	const lightTheme = { body: '#FFF', text: '#363537' };
	const darkTheme = { body: '#363537', text: '#FAFAFA' };

	// Characters variables and localStorage config
	const localFavorites = JSON.parse(localStorage.getItem('favorites')) ?? [];
	const [characters, setCharacters] = useState([]);
	const [favorites, setFavorites] = useState(localFavorites);
	useEffect(() => {
		localStorage.setItem('favorites', JSON.stringify(favorites));
	}, [favorites]);

	// API call to get the data we want
	function getCharacters() {
		if (characters.length === 0 || !characters) {
			// checks if array is already filled to not make too many calls
			axios.get(`${apiUrl}character`, config).then((r) => setCharacters(r.data.docs));
		}
	}

	// Adds the new character to our favorites
	function addToFavorites(character) {
		setFavorites(favorites.concat([character]));
	}

	// Removes the characters from our favorites
	function removeFromFavorites(favorite) {
		setFavorites(
			favorites.filter(function (char) {
				return char._id !== favorite._id;
			})
		);
	}

	// Checks if the character is in our favorites to disable the favorite button
	function containsCharacter(char, favorites) {
		let i;
		for (i = 0; i < favorites.length; i++) if (favorites[i]._id === char._id) return true;
		return false;
	}

	function Favorites() {
		return (
			<div>
				<p>Choose your favorite characters form the story easily with this simple app!</p>
				<h3>Favorites</h3>
				{favorites.map((favorite) => {
					return (
						<div className="main">
							<div className="right">
								<Button type="secondary" onPress={() => removeFromFavorites(favorite)}>
									delete
								</Button>
							</div>
							<div className="left">
								<p key={favorite._id}> {favorite.name}</p>
							</div>
						</div>
					);
				})}
			</div>
		);
	}

	function Home() {
		getCharacters();
		return (
			<div>
				<p>Choose your favorite characters form the story easily with this simple app!</p>
				<h3>Characters</h3>
				{characters.map((character) => {
					return (
						<div className="main">
							<div className="right">
								<Button
									type="primary"
									disabled={containsCharacter(character, favorites)}
									onPress={() => addToFavorites(character)}
								>
									favorite
								</Button>
							</div>
							<div className="left">
								<p key={character._id}> {character.name}</p>
							</div>
						</div>
					);
				})}
			</div>
		);
	}

	return (
		<ThemeProvider theme={theme ? lightTheme : darkTheme}>
			<>
				<GlobalStyles />
				<div className="App">
					<header className="App-header">
						<img src={pic} alt="header" />
						<h1>The Lord Of The Rings</h1>
						<Router>
							<div>
								<Button type="secondary" onPress={() => setTheme(!theme)}>
									Switch Theme
								</Button>
								<Link className="buttons" to="/">
									<Button type="primary">Home</Button>
								</Link>
								<Link className="buttons" to="/favorites">
									<Button type="primary">Favorites</Button>
								</Link>
								<Routes>
									<Route exact path="/" element={<Home />} />
									<Route exact path="/favorites" element={<Favorites />} />
								</Routes>
							</div>
						</Router>
					</header>
				</div>
			</>
		</ThemeProvider>
	);
}

export default App;
